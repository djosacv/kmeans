/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "kview.h"

#include <QtGui/QHBoxLayout>
#include <QtGui/QToolBar>
#include <QtGui/QFileDialog>
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtGui/QLabel>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    resize( 800, 600 );
    QWidget *centralWidget = new QWidget( this );
    setCentralWidget( centralWidget );

    QBoxLayout *mainLayout = new QVBoxLayout( this );
    centralWidget->setLayout( mainLayout );

    QGridLayout *rowLayout = new QGridLayout( centralWidget );
    mainLayout->addLayout( rowLayout );

    QLabel *xLabel = new QLabel( tr( "Column on <i>x</i> axis:"), centralWidget );
    rowLayout->addWidget( xLabel, 0, 0 );
    m_xBox = new QSlider( centralWidget );
    m_xBox->setOrientation( Qt::Horizontal );
    m_xBox->setMaximum( 0 );
    rowLayout->addWidget( m_xBox, 0, 1 );
    QLabel *xValue = new QLabel( "0", centralWidget );
    rowLayout->addWidget( xValue, 0, 2 );
    xLabel->setBuddy( m_xBox );
    connect( m_xBox, SIGNAL( valueChanged(int) ), xValue, SLOT( setNum(int) ) );


    QLabel *yLabel = new QLabel( tr( "Column on <i>y</i> axis:"), centralWidget );
    rowLayout->addWidget( yLabel, 1, 0 );
    m_yBox = new QSlider( centralWidget );
    m_yBox->setOrientation( Qt::Horizontal );
    m_yBox->setMaximum( 0 );
    rowLayout->addWidget( m_yBox, 1, 1 );
    QLabel *yValue = new QLabel( "0", centralWidget );
    rowLayout->addWidget( yValue, 1, 2 );
    yLabel->setBuddy( m_yBox );
    connect( m_yBox, SIGNAL( valueChanged(int) ), yValue, SLOT( setNum(int) ) );

    rowLayout->setColumnStretch( 3, 1 );

    QLabel *kNumberLabel = new QLabel( tr( "Clusters count: " ), centralWidget );
    rowLayout->addWidget( kNumberLabel, 0, 4 );
    m_kNumberBox = new QSpinBox( centralWidget );
    m_kNumberBox->setMinimum( 1 );
    m_kNumberBox->setMaximum( 32 );
    m_kNumberBox->setValue( 3 );
    rowLayout->addWidget( m_kNumberBox, 0, 5 );
    kNumberLabel->setBuddy( m_kNumberBox );

    QLabel *algorithmLabel = new QLabel( tr( "Clustering algorithm: " ), centralWidget );
    rowLayout->addWidget( algorithmLabel, 1, 4 );
    m_algorithmBox = new QComboBox( centralWidget );
    m_algorithmBox->addItems( QStringList()
                              << "k-means"
                              << "k-medoids" );
    rowLayout->addWidget( m_algorithmBox, 1, 5 );
    algorithmLabel->setBuddy( m_algorithmBox );

    QLabel *tickIntervalLabel = new QLabel( tr( "Tick interval (speed): " ), centralWidget );
    rowLayout->addWidget( tickIntervalLabel, 2, 4 );
    m_tickIntervalSlider = new QSlider( centralWidget );
    m_tickIntervalSlider->setMinimum( 0 );
    m_tickIntervalSlider->setMaximum( 3000 );
    m_tickIntervalSlider->setOrientation( Qt::Horizontal );
    rowLayout->addWidget( m_tickIntervalSlider, 2, 5 );
    m_tickIntervalSpinBox = new QSpinBox( centralWidget );
    m_tickIntervalSpinBox->setMinimum( 0 );
    m_tickIntervalSpinBox->setMaximum( 3000 );
    rowLayout->addWidget( m_tickIntervalSpinBox, 2, 6 );
    connect( m_tickIntervalSlider, SIGNAL( valueChanged( int ) ),
             m_tickIntervalSpinBox, SLOT( setValue( int ) ) );
    connect( m_tickIntervalSpinBox, SIGNAL( valueChanged( int ) ),
             m_tickIntervalSlider, SLOT( setValue( int ) ) );

    m_startButton = new QPushButton( tr( "Run!" ), centralWidget );
    rowLayout->addWidget( m_startButton, 2, 7 );
    m_startButton->setEnabled( false );
    connect( m_startButton, SIGNAL( clicked() ), this, SLOT( onRunClicked() ) );

    rowLayout->setColumnStretch( 8, 1 );


    KView *view = new KView( centralWidget );
    mainLayout->addWidget( view );

    m_dataModel = new KModel( this );
    view->setModel( m_dataModel );

    connect( m_dataModel, SIGNAL( crunching() ), this, SLOT( blockUi() ) );
    connect( m_dataModel, SIGNAL( crunchingDone() ), this, SLOT( unblockUi() ) );

    QToolBar *mainToolbar = addToolBar( tr( "Main Toolbar" ) );
    mainToolbar->setFloatable( false );
    mainToolbar->setMovable( false );
    mainToolbar->setAllowedAreas( Qt::TopToolBarArea );

    mainToolbar->addAction( tr( "Load dataset" ), this, SLOT( loadDataset() ) );
}

MainWindow::~MainWindow()
{
    
}

void
MainWindow::loadDataset()
{
    QString filePath = QFileDialog::getOpenFileName( this, "Open Data Set" );
    if( filePath.isEmpty() )
        return;

    QFile data( filePath );
    if( data.open( QFile::ReadOnly ) )
    {
        QTextStream in( &data );
        DataSet dataSet;
        QString line;

        /*
         * The file format is defined as follows.
         * # starts a comment line
         * An entry line is valid if it has comma separated number values followed by a
         * single comma separated string value.
         * An entry line must have at lest one number value and exactly one string value
         * at the end.
         * Every entry line has the exact same number of columns as the first non-comment
         * line.
         */
        bool isFirstDataLine = true;
        for(;;)
        {
            line = in.readLine();
            if( line.isEmpty() )
                break;
            if( line.startsWith( '#' ) )
                continue;
            QStringList broken = line.split( ',' );

            QVector< double > aPoint;
            for( int i = 0; i < broken.length() - 1; ++i )
                aPoint << broken.at( i ).toDouble();

            dataSet.points.append( aPoint );

            dataSet.labels << broken.last();
        }

        m_dataModel->loadData( dataSet );

        m_xBox->setMaximum( m_dataModel->columnCount() - 1 );
        m_yBox->setMaximum( m_dataModel->columnCount() - 1 );

        connect( m_xBox, SIGNAL( valueChanged(int) ), m_dataModel, SLOT( setXColumn( int ) ) );
        connect( m_yBox, SIGNAL( valueChanged(int) ), m_dataModel, SLOT( setYColumn( int ) ) );
        if( m_dataModel->columnCount() > 1 )
            m_yBox->setValue( 1 );

        m_startButton->setEnabled( true );
    }
}

void
MainWindow::onRunClicked()
{
    qDebug() << "Run clicked";
    if( m_dataModel )
    {
        if( m_dataModel->isCrunching() )
            m_dataModel->stop();
        else
            m_dataModel->run( m_algorithmBox->currentIndex(),
                              m_kNumberBox->value(),
                              m_tickIntervalSpinBox->value() );
    }
}

void
MainWindow::blockUi()
{
    m_startButton->setText( tr( "Stop!" ) );
    m_kNumberBox->setEnabled( false );
    m_algorithmBox->setEnabled( false );
    m_xBox->setEnabled( false );
    m_yBox->setEnabled( false );
    m_tickIntervalSlider->setEnabled( false );
    m_tickIntervalSpinBox->setEnabled( false );
}

void
MainWindow::unblockUi()
{
    m_startButton->setText( "Run!" );
    m_kNumberBox->setEnabled( true );
    m_algorithmBox->setEnabled( true );
    m_xBox->setEnabled( true );
    m_yBox->setEnabled( true );
    m_tickIntervalSlider->setEnabled( true );
    m_tickIntervalSpinBox->setEnabled( true );
}
