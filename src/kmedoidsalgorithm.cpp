/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "kmedoidsalgorithm.h"

#include <QtCore/QTime>

KMedoidsAlgorithm::KMedoidsAlgorithm( const DataSet &data,
                                      QVector<int> &clusters,
                                      QVector<QVector<double> > &currentMeans,
                                      int clusterCount,
                                      unsigned int interval,
                                      QObject *parent )
    : AbstractClusteringAlgorithm( data,
                                   clusters,
                                   currentMeans,
                                   clusterCount,
                                   interval,
                                   parent )
{
}

void
KMedoidsAlgorithm::run()
{
    qDebug() << "Begin kMedoids";
    qsrand( QTime::currentTime().msec() );  //whoops, separate thread so we have to seed
    //let's randomly pick a few means...
    for( int j = 0; j < m_clusterCount; ++j )
    {
        int randomRow = qrand() % m_data.points.size();
        qDebug() << "picket at random row " << randomRow;
        //a vector of points
        m_currentMeans.append( QVector< double >( m_data.points.at( randomRow ) ) );
    }

    //let's assign every point to exactly one mean...
    for( int i = 0; i < m_data.points.size(); ++i )
    {
        m_clusters[ i ] = pickMean( m_data.points.at( i ), m_currentMeans );
                /*the cluster-index of the meanpoint that minimizes the distance*/
    }

    bool clustersChanged = true;
    int iterations = 0;
    while( clustersChanged )
    {
        if( m_stopAsap )
            break;

        clustersChanged = false; //let's assume nothing will change in this iteration

        for( int j = 0; j < m_clusterCount; ++j )
        {
            m_currentMeans[ j ] = findMedoid( m_data, m_clusters, j, m_currentMeans.at( j ) );
        }
        for( int i = 0; i < m_data.points.size(); ++i )
        {
            int newClusterId = pickMean( m_data.points.at( i ), m_currentMeans );

            if( newClusterId != m_clusters[ i ] )
            {
                clustersChanged = true;
                m_clusters[ i ] = newClusterId;
            }
        }
        iterations++;

        emit stepTaken();

        if( m_interval > 0)
            msleep( m_interval );
    }
    qDebug() << "Took " << iterations << " steps!";
}

int
KMedoidsAlgorithm::pickMean( const QVector<double> &point,
                             const QVector<QVector<double> > &currentMeans) const
{
    int bestMean = 0;
    double bestMeanDistance = std::numeric_limits< double >::infinity();

    for( int k = 0; k < currentMeans.size(); ++k )
    {
        double newDistance = distance( point, currentMeans.at( k ) );
        if( newDistance < bestMeanDistance )
        {
            bestMeanDistance = newDistance;
            bestMean = k;
        }
    }
    return bestMean;
}

QVector< double >
KMedoidsAlgorithm::findMedoid( const DataSet &data,
                               const QVector<int> &clusters,
                               int clusterId,
                               QVector<double> oldMedoid) const
{
    QVector< double > candidateMedoid = oldMedoid;

    double cost = 0.0; //let's compute the cost of the current situation...
    for( int i = 0; i < data.points.size(); ++i )
    {
        if( clusters.at( i ) != clusterId )
            continue;
        cost += distance( oldMedoid, data.points.at( i ) );
    }

    //and now for each non-medoid, we compute the cost as if it were the medoid
    for( int i = 0; i < data.points.size(); ++i )
    {
        if( clusters.at( i ) != clusterId )
            continue;

        double newCost = 0.0;
        for( int j = 0; j < data.points.size(); ++j )
        {
            if( clusters.at( j ) != clusterId )
                continue;

            newCost += distance( data.points.at( i ), data.points.at( j ) );
        }

        if( newCost < cost )
            candidateMedoid = data.points.at( i );
    }
    return candidateMedoid;
}
