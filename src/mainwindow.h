/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "kmodel.h"

#include <QtGui/QMainWindow>
#include <QtCore/QPointer>
#include <QtGui/QSlider>
#include <QtGui/QSpinBox>
#include <QtGui/QComboBox>
#include <QtGui/QPushButton>

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void loadDataset();
    void onRunClicked();

    void blockUi();
    void unblockUi();

private:
    QPointer< KModel > m_dataModel;

    QSlider *m_xBox;
    QSlider *m_yBox;

    QSpinBox *m_kNumberBox;
    QComboBox *m_algorithmBox;

    QPushButton *m_startButton;
    QSlider *m_tickIntervalSlider;
    QSpinBox *m_tickIntervalSpinBox;
};

#endif // MAINWINDOW_H
