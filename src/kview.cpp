/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "kview.h"

#include <qwt/qwt_series_data.h>
#include <qwt/qwt_plot_spectrocurve.h>
#include <qwt/qwt_plot_curve.h>
#include <qwt/qwt_symbol.h>

#include <QHBoxLayout>

KView::KView( QWidget *parent )
    : QWidget( parent )
{
    m_pl = new QwtPlot( QString(), this );

    QBoxLayout *mainLayout = new QHBoxLayout( this );
    mainLayout->addWidget( m_pl );


    m_pl->setAxisScale( QwtPlot::xBottom, 0.0, 1.0 );
    m_pl->setAxisScale( QwtPlot::yLeft, 0.0, 1.0 );
    m_pl->updateAxes();
    m_pl->setAxisAutoScale( QwtPlot::xBottom, true );
    m_pl->setAxisAutoScale( QwtPlot::yLeft, true );

}

void
KView::setModel( KModel *model )
{
    m_model = model;
    connect( model, SIGNAL( dataChanged() ), this, SLOT( update() ) );
}

void
KView::update()
{
    if( !m_model )
        return;

    qDebug() << "Update beginning";

    m_pl->detachItems();

    //let's plot the data points first. this stuff will change color but never position.
    QwtPlotSpectroCurve *points = new QwtPlotSpectroCurve();

    points->setPenWidth( 4 );
    points->setColorRange( QwtInterval(0, m_model->clusterCount() -1 ) );
    points->setRenderHint( QwtPlotSpectroCurve::RenderAntialiased, true );

    QVector< QwtPoint3D > data3d;
    for( int row = 0; row < m_model->rowCount(); ++row )
        data3d.append( QwtPoint3D( m_model->data( row ).first,
                                   m_model->data( row ).second,
                                   m_model->cluster( row ) ) );

    QwtPoint3DSeriesData *psd = new QwtPoint3DSeriesData( data3d );

    points->setData( psd );

    points->setItemAttribute( QwtPlotItem::AutoScale, true );
    points->attach( m_pl );

    //and now let's paint the current means/medioids. these start out random and then move around.
    QwtPlotCurve *means = new QwtPlotCurve();
    means->setStyle( QwtPlotCurve::NoCurve );

    QwtSymbol *symbol = new QwtSymbol( QwtSymbol::Star1 );
    symbol->setSize( 13, 13 );
    means->setSymbol( symbol );
    qDebug() << "ok here";

    QVector< QPointF > meanPoints;
    for( int clusterId = 0; clusterId < m_model->meansCount(); ++clusterId )
        meanPoints.append( QPointF( m_model->mean( clusterId ).first,
                                    m_model->mean( clusterId ).second ) );

    QwtPointSeriesData *msd = new QwtPointSeriesData( meanPoints );

    means->setData( msd );
    means->attach( m_pl );
    //finally, refresh the view!
    m_pl->update();
    m_pl->replot();

    qDebug() << points->colorRange().maxValue();
}
