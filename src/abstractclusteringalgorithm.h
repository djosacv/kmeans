/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ABSTRACTCLUSTERINGALGORITHM_H
#define ABSTRACTCLUSTERINGALGORITHM_H

#include "kmodel.h"

#include <QtCore/QVector>
#include <QtCore/QThread>
#include <limits>
#include <cmath>

class DataSet;

class AbstractClusteringAlgorithm : public QThread
{
    Q_OBJECT
public:
    AbstractClusteringAlgorithm( const DataSet &data,
                                 QVector< int > &clusters,
                                 QVector< QVector< double > > &currentMeans,
                                 int clusterCount,
                                 unsigned int interval,
                                 QObject *parent = 0 )
        : m_data( data)
        , m_clusters( clusters )
        , m_currentMeans( currentMeans )
        , m_clusterCount( clusterCount )
        , m_interval( interval )
        , QThread( parent )
        , m_stopAsap( false ) {}

    virtual void run() = 0;

    void stopAsap() { m_stopAsap = true; }

signals:
    void stepTaken();

protected:
    const DataSet &m_data;
    QVector< int > &m_clusters;
    QVector< QVector< double > > &m_currentMeans;
    int m_clusterCount;
    unsigned int m_interval;
    bool m_stopAsap;

    static double distance( const QVector< double > &first,
                            const QVector< double > &second )
    {
        if( first.size() != second.size() )
            return std::numeric_limits< double >::quiet_NaN();

        double accumulator = 0;

        for( int i = 0; i < first.size(); ++i )
        {
            accumulator += ( first.at( i ) - second.at( i ) )
                         * ( first.at( i ) - second.at( i ) );
        }

        return sqrt( accumulator );
    }

};

#endif // ABSTRACTCLUSTERINGALGORITHM_H
